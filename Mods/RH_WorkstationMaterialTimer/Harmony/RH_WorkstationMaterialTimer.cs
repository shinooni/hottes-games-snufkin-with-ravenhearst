﻿using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace RH_WorkstationMaterialTimer
{
    public class RH_WorkstationMaterialTimer_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_WorkstationMaterialInputGrid))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_WorkstationMaterialInputGridUpdate
    {
        static void Postfix(XUiC_WorkstationMaterialInputGrid __instance, ref float _dt)
        {
            int num = 0;
            while (num < __instance.workstationData.TileEntity.Input.Length && num < __instance.itemControllers.Length)
            {
                float timerForSlot = __instance.workstationData.TileEntity.GetTimerForSlot(num);
                if (timerForSlot > 0f)
                {
                    var adjustedTime = GetTotalTime(__instance.workstationData.TileEntity, num) + timerForSlot + 0.95f;
                    ((XUiC_ItemStack)__instance.itemControllers[num]).timer.IsVisible = true;
                    ((XUiC_ItemStack)__instance.itemControllers[num]).timer.Text = string.Format("{0}:{1}:{2}", Mathf.Floor((adjustedTime) / 3600f).ToCultureInvariantString("00"), Mathf.Floor((adjustedTime%3600f) / 60f).ToCultureInvariantString("00"), Mathf.Floor((adjustedTime) % 60f).ToCultureInvariantString("00"));
                }
                else
                {
                    ((XUiC_ItemStack)__instance.itemControllers[num]).timer.IsVisible = false;
                }
                num++;
            }
            __instance.workstationData.GetIsBurning();
        }

        private static float GetTotalTime(TileEntityWorkstation tileEntity, int slotId)
        {
            ItemClass forId = ItemClass.GetForId(tileEntity.input[slotId].itemValue.type);
            float time = 0f;

            if (forId != null)
            {
                for (int j = 0; j < tileEntity.materialNames.Length; j++)
                {
                    if (forId.MadeOfMaterial.ForgeCategory != null && forId.MadeOfMaterial.ForgeCategory.EqualsCaseInsensitive(tileEntity.materialNames[j]))
                    {
                        ItemClass itemClass = ItemClass.GetItemClass("unit_" + tileEntity.materialNames[j], false);
                        if (itemClass != null && itemClass.MadeOfMaterial.ForgeCategory != null)
                        {
                            time = (float)forId.GetWeight() * ((forId.MeltTimePerUnit > 0f) ? forId.MeltTimePerUnit : 1f);
                            if (tileEntity.isModuleUsed[0])
                            {
                                for (int k = 0; k < tileEntity.tools.Length; k++)
                                {
                                    float num2 = 1f;
                                    tileEntity.tools[k].itemValue.ModifyValue(null, null, PassiveEffects.CraftingSmeltTime, ref time, ref num2, FastTags.Parse(forId.Name), true);
                                    time *= num2;
                                }
                            }
                        }
                    }
                }
            }

            if(time > 0f && tileEntity.input[slotId].count >= 1)
            {
                return time * (tileEntity.input[slotId].count - 1);
            }

            return time;
        }
    }

    [HarmonyPatch(typeof(XUiC_WorkstationWindowGroup))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_WorkstationWindowGroup
    {
        static void Postfix(XUiC_WorkstationWindowGroup __instance, ref float _dt)
        {
            if (__instance.fuelWindow != null && __instance.craftingQueue != null && __instance.burnTimeLeft != null && __instance.WorkstationData != null)
            {
                float totalBurnTimeLeft = __instance.WorkstationData.GetTotalBurnTimeLeft();
                __instance.burnTimeLeft.Text = string.Format("{0}:{1}:{2}", Mathf.Floor((totalBurnTimeLeft) / 3600f).ToCultureInvariantString("00"), Mathf.Floor((totalBurnTimeLeft % 3600f) / 60f).ToCultureInvariantString("00"), Mathf.Floor((totalBurnTimeLeft) % 60f).ToCultureInvariantString("00"));
            }
        }
    }
}

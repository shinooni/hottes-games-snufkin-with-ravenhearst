Experimental work in progress version of the boating modlet mow at Version 4.



Launch

Place the boat on water in a 'shallow water' area, then add fuel to boat. The Small boat is super economical on fuel so fuel will last a very very long time.

The boat will behave a bit irratically if placed on land and will damage player made blocks, it will probably move about a bit on its own but will work very stable on 
water.

Controls

Usual controls for vehicles except space bar and c control the up angle (same as a gyro) which you may need to adjust slightly as you travel on water.  
If you press s whilst going forward it will slow the boat down and eventually very slow reverse will kick in this replaces spacebar as the brake key.

Boat can be picked up into inventory, if you sink it at any point, although be careful if you sink it too far away from the shores as it could be a long swim back.

The Boats are craftable in a standard workbench with no progression needed as the boats are intended as an early game version at the cost of resources and actually 
finding a working workbench in early game would be a bonus and allow you to make the boatd a lot sooner.  The recipe is simple and uses in game resources.


Along the way hopefully we can expand this to have some bigger boats and different types of watercraft such as amphibious vehicles, a canoe, a raft , small sail boat and
perhaps even a jetski. 

There are few known issues and glitches that need work arounds but hopefully these can be worked out along the way.

Updates Version 2
Fixed exit position thanks to DUST2DEATH amd the APC exit co-ordinates ... 

Update Version 3 
Updated to A18 compatible xml physics and boats now placeable in water.

Update Version 4
Fixed momentum and boats will slowly come to a halt and stop ... they stop quicker with pressing the S key 
Both boats can stop on water just make sure boats are level in the water and they wont drift away ...useful for fishing mods and the like.
Fixed placing in water ..boats were slow to come to surface.
Boat stabilty improved and stopped the random sinking when tilited to much.
Added a second seat to the flat bottomed boat.
Adjusted turn rates and speeds to more suitable levels (subjective) 




More to come !

Happy Boating in A18

Ragsy and Guppycur !!

 

